package com.example.work5

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.Toast
import kotlinx.android.synthetic.main.activity_authentication.*

class AuthenticationActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_authentication)
        init()
    }
    private fun init(){
        signInButton.setOnClickListener(){
            signIn()
        }

    }

    private fun signIn(){
        val email = emailEditText.text.toString()
        val password = passwordEditText.text.toString()
        val intent = Intent (this, ProfileActivity::class.java )

        if(email.isNotEmpty() && password.isNotEmpty()){
            progressBar.visibility = View.VISIBLE
            signInButton.isClickable = false
            startActivity(intent)
        }
        else
            Toast.makeText(this, "Please fill all fields",Toast.LENGTH_SHORT).show()
    }
}