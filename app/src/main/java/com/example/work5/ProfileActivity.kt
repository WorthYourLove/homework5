package com.example.work5

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.bumptech.glide.Glide
import kotlinx.android.synthetic.main.activity_profile.*

class ProfileActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_profile)
        init()
    }
    private fun init(){
        setImages()

    }
    private fun setImages(){
        Glide.with(this)
                .load("https://i.pinimg.com/564x/5e/62/62/5e626287604958da503f801af3d3fcd1.jpg")
                .placeholder(R.mipmap.background)
                .into(imageView1)

        Glide.with(this)
                .load("https://i.pinimg.com/564x/7b/56/0b/7b560b8824b8ae3ee5263047c54e4159.jpg")
                .placeholder(R.mipmap.background)
                .into(imageView2)

        }
}
